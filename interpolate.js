export function interpolate(from, to, duration, method = ease, callback) {
  let start;
  return (timestamp) => {
    if (start === undefined) {
      start = timestamp;
    }
    const t = normalize(start, timestamp, duration);
    if (timestamp - start > duration) {
      callback?.(from + (to - from) * method(t));
    }
    return from + (to - from) * method(t);
  };
}

function normalize(start, current, max) {
  return Math.min(1, (current - start) / max);
}

export function linear(t) {
  return t;
}

export function ease(t) {
  return t < 0.5 ? 2 * t * t : -1 + (4 - 2 * t) * t;
}

export function easeIn(t) {
  return t * t;
}

export function easeOut(t) {
  return t * (2 - t);
}
