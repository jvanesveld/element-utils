/**
 * Incrementing id generator
 */
let counter = -1;
export default () => {
  counter++;
  return `unique_id_${counter}`;
};
