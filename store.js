/**
 * Super simple redux like store
 */
export class Store {
  callbacks = [];
  state;

  constructor(init_state) {
    this.state = init_state;
  }

  setState(mutator) {
    const old_state = this.state;
    this.state = mutator(this.state);
    this.triggerChange(old_state);
  }

  listen(callback) {
    this.callbacks.push(callback);
    callback(this.state, this.state);
    return () => {
      const index = this.callbacks.indexOf(callback);
      this.callbacks.splice(index, 1);
    };
  }

  triggerChange(old_state) {
    this.callbacks.forEach((callback) => callback(old_state, this.state));
  }
}
