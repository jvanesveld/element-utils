/**
 * Call an async function where the last argument is a callback
 * as if it returns a promise
 * @param {*} method Method to call
 * @param  {...any} options Arguments to pass
 */
export function asyncCallback(method, ...options) {
  return new Promise((resolve, _) => {
    method(...options, (...args) => resolve(...args));
  });
}
