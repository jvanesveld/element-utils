/**
 * CustomElement class, used for easier data binding
 */
export class CustomElement extends HTMLElement {
  /**
   * Initializes a property as an input, triggering `this.inputChanged` and `onChange` when it's value is changed
   *
   * Attribute options:
   * - `sync`: synchronizes property with the attribute
   * - `attrName`: used if attribute is not the same as the property name
   * - `isBool`: allows attribute to be interpreted as a boolean attribute
   * - `initCheck`: Check for difference between the property and attribute on init
   *
   * !!To listen for attribute changes remember to add it to `observedAttributes` !!
   *
   * @param {*} name The name of the property on this, like `this[name]`
   * @param {*} attrOptions Attribute options
   */
  initInput(
    name,
    attrOptions = {
      sync: false,
      attrName: undefined,
      isBool: false,
      initCheck: true,
    },
    onChange = (oldValue, newValue) => {}
  ) {
    let initUpdate = {
      required: false,
      oldValue: this[name],
      newValue: this[name],
    };
    this._inputValues = this._inputValues || {}; // Todo: find better place to store these
    this._inputCalbacks = this._inputCalbacks || {}; // Todo: find better place to store these
    attrOptions.attrName = attrOptions.attrName || name;
    if (attrOptions.sync && !this.attributeChangedCallback) {
      console.warn(
        `The attributeChangedCallback of ${this.tagName} is undefined`
      );
    }
    if (attrOptions.sync && this.isConnected) {
      let attrValue = this.getAttribute(attrOptions.attrName);
      initUpdate.required = attrValue && initUpdate.oldValue != attrValue;
      initUpdate.newValue = this.getAttribute(attrOptions.attrName);
      const current = this.getAttribute(attrOptions.attrName);
      if (!current && this[name] != undefined) {
        this.setAttribute(attrOptions.attrName, this[name]);
      }
    } else {
      this._inputValues[name] = this[name];
    }
    this._inputCalbacks[attrOptions.attrName] = onChange;
    Object.defineProperty(this, name, {
      get() {
        if (attrOptions.isBool) {
          return attrOptions.sync
            ? this.getAttribute(attrOptions.attrName) === ""
            : this._inputValues[name];
        }
        return attrOptions.sync
          ? this.getAttribute(attrOptions.attrName) || this._inputValues[name]
          : this._inputValues[name];
      },
      set(newValue) {
        const oldValue = this[name];
        if (
          oldValue != newValue ||
          JSON.stringify(oldValue) !== JSON.stringify(newValue)
        ) {
          if (attrOptions.sync) {
            if (attrOptions.isBool) {
              if (newValue) {
                this.setAttribute(attrOptions.attrName, "");
              } else {
                this.removeAttribute(attrOptions.attrName);
              }
            } else {
              this.setAttribute(attrOptions.attrName, newValue);
            }
          } else {
            this._inputValues[name] = newValue;
            onChange(oldValue, newValue);
            this.inputChanged?.(name, oldValue, newValue);
          }
        }
      },
    });
    if (initUpdate.required && attrOptions.initCheck) {
      this._inputCalbacks?.[attrOptions.attrName]?.(
        initUpdate.oldValue,
        initUpdate.newValue
      );
      this.inputChanged?.(
        attrOptions.attrName,
        initUpdate.oldValue,
        initUpdate.newValue
      );
    }
  }

  attributeChangedCallback(name, oldValue, newValue) {
    if (this.isConnected) {
      this._inputCalbacks?.[name]?.(oldValue, newValue);
      this.inputChanged?.(name, oldValue, newValue);
    }
  }

  inputChanged(name, oldValue, newValue) {}
}
