/**
 * Super simple redux like store
 * Wait for animation frames and applies updates in batches
 */
export class FrameStore {
  callbacks = [];
  mutators = [];
  pending = false;
  state;

  constructor(init_state) {
    this.state = init_state;
  }

  setState(mutator) {
    this.mutators.push(mutator);
    if (!this.pending) {
      this.pending = true;
      requestAnimationFrame(() => this.triggerChange());
    }
  }

  listen(callback) {
    this.callbacks.push(callback);
    callback(this.state, this.state);
    return () => {
      const index = this.callbacks.indexOf(callback);
      this.callbacks.splice(index, 1);
    };
  }

  applyMutators() {
    while (this.mutators.length > 0) {
      this.state = this.mutators.splice(0, 1)[0](this.state);
    }
  }

  triggerChange() {
    this.pending = false;
    const old_state = this.state;
    this.applyMutators();
    this.callbacks.forEach(async (callback) => callback(old_state, this.state));
  }
}
