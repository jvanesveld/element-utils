import { render as lit_render } from "./lit-html/lit-html.js";
export { html } from "./lit-html/lit-html.js";

/**
 * Renders a template string to the shadowroot of an element
 * @param target Target element
 * @param template Lit-html template string
 * @param shadowOptions Shadow root options
 */
export function renderShadow(
  target,
  template,
  shadowOptions = { mode: "open" }
) {
  if (!target.shadowRoot) {
    target.attachShadow(shadowOptions);
  }
  lit_render(template, target.shadowRoot);
}

/**
 * Renders a template string to an element
 * @param target Target element
 * @param template Lit-html template string
 */
export function render(target, template) {
  lit_render(template, target);
}
